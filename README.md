# Appunti di Version Control di Federica Nicolò

## Parte Teorica

### Che cos'è il version control?
Il version control è un sistema che consente di tenere traccia delle modifiche apportate ad un progetto di sviluppo software nel tempo e ai membri del team di collaborare mantenendo una cronologia delle modifiche apportate al codice sorgente e alle risorse del progetto.

### Differenze tra Git e altri sistemi
Concettualmente la maggior parte degli altri sistemi salvano l’informazione come una lista di modifiche ai file e  considerano le informazioni che memorizzano come un insieme di file, con le relative modifiche fatte nel tempo (controllo di versione su base delta).

Git non considera i dati né li registra in questo modo. Git considera i propri dati più come una sequenza di istantanee (snapshot) di un mini filesystem. Con Git, ogni volta che registri (commit), o salvi lo stato del tuo progetto, si crea un’immagine di tutti i file in quel momento, salvando un riferimento allo snapshot. Per essere efficiente, se alcuni file non sono cambiati, Git non li risalva, ma crea semplicemente un collegamento al file precedente già salvato. Git considera i propri dati più come un flusso di istantanee.

### Che cos'è in breve Git?
E' un sistema di controllo versione distribuito, che permette di tenere traccia delle modifiche apportate a un insieme di file nel tempo (gestendone la storia), in modo da poter lavorare in modo collaborativo e recuperare facilmente versioni precedenti dei file.

Viene usato nei progetti software, nei quali molte persone possono lavorare contemporaneamente sullo stesso codice sorgente: per questo Git mette a disposizione i "branch", ovvero delle ramificazioni delle stesso codice con i quali ogni membro di un team può lavorare separatamente.


## Parte Pratica

### Comandi base di GIT

` git <qualcosa> --help `

Questo comando apre il manuale e ti dice cosa fa qualcosa con le sue declinazioni. 

` git init [<directory>] `

Questo comando crea un repository Git vuoto, ovvero una cartella .git nella directory specificata. Se non viene specificata alcuna directory lo crea in quella corrente. Da qui in poi git terrà traccia dei cambienti effettuati.

` git config `

- (per tutti i repository)   
```git config --global user.name "Your Name"```  
```git config --global user.email "youremail@yourdomain.com"```
- (per singoli repository)  
```git config user.name "Your Name"```  
```git config user.email "youremail@yourdomain.com"```


Questo comando serve per specificare alcuni parametri di default come nome e email che verranno usati in seguito da git.

` git add `

Questo comando aggiorna l'indice del working tree per preparare il contenuto del commit successivo. L '"indice" contiene un'istantanea del contenuto del working tree, ed è questa istantanea che viene presa come contenuto del commit successivo. Pertanto, dopo aver apportato modifiche al working tree e prima di eseguire il comando commit, è necessario utilizzare il comando add per aggiungere file nuovi o modificati all'indice.

` git commit [-m <qualcosa>] `

Questo comando salva le modifiche nella staging area nel repository. Ogni commit viene accompagnato da un messaggio di commit che descrive brevemente le modifiche apportate. Se viene specificato -m il messaggio dovrà essere breve e verrà inserito al posto di qualcosa, altrimenti si apre un editor che fa inserire messaggi di commit più lunghi.

` git status `

Questo comando mostra lo stato del repository, ovvero quali file sono stati modificati, quali sono pronti per essere committati e quali sono stati esclusi dalla gestione di versione.

` git log `

Questo comando mostra la lista dei commit effettuati nel repository, insieme alle relative informazioni come autore, data e messaggio di commit.

- ` git log --graph `  
Per visualizzare la diramazione dei vari branch in un repository Git, puoi utilizzare il comando git log con l'opzione --graph.
- ` git log --graph --decorate`  
Visualizza anche i nomi dei branch e dei tag nei commit corrispondenti nel grafico.

` git branch `

Questo comando mostra l'elenco dei branch presenti nel repository.

- ` git branch -m [<oldbranch>] <newbranch> `  
Consente di rinominare oldbranch in newbranch.

` git diff `

Questo comando mostra le modifiche tra due punti, che possono essere commit, branch o file.

` git checkout <nomebranch>`

Questo comando permette di spostarsi tra i diversi branch presenti nel repository. È possibile utilizzare git checkout anche per ripristinare una versione precedente dei file.

- ` git checkout -b ＜new-branch＞`
Questo comando crea un nuovo branch e ci sposta su di esso.

` git switch <nomebranch>`

Questo comando permette di spostarsi tra i diversi branch presenti nel repository.

- ` git switch -c ＜new-branch＞`
Questo comando crea un nuovo branch e ci sposta su di esso.
 

### Git checkout o git switch?

In generale, git switch è preferibile a git checkout quando si cambia branch perché è più sicuro impedendo il "detached HEAD state".

Git checkout è ancora utile in alcuni casi, ad esempio quando si desidera creare un nuovo branch dal commit corrente, creare una nuova branch partendo da una branch esistente o quando si vuole spostarsi tra branch e file contemporaneamente.

### Git merge e i conflitti

` git merge <nomebranch>`

Questo comando consente di unire le modifiche di un branch in un altro. Ovvero prende le modifiche apportate in un branch e le unisce con quelle presenti in un altro branch, creando un nuovo commit che rappresenta la fusione dei due branch.

Esempio di utilizzo:  
` git checkout main `  
` git merge <nome-branch> `  
Mi metto prima nel main e poi faccio un merge con un altro branch.

Quando si verifica un conflitto, per esempio quando si sta modificando in due modi diversi uno stesso file, git ce lo segnala. 

"Automatic merge failed; fix conflicts and then commit the result."

Cosa fare in questi casi?

1. Fare ` git status ` per ottenere ulteriori informazioni
2. Esaminare con ` cat ` il file che dà il conflitto.
3. Il file sarà simile a questo   
> ```
> <<<<<<< HEAD  
> some content 
> =======  
> totally different content 
> >>>>>>> new_branch 
> ```
4. Scegliere quindi quale delle due parti cancellare (o se si deve integrarne una con l'altra), quindi cancellare la parte di HEAD e di new_branch e i segni di uguale. Quindi salvare il file.
5. Aggiungere con ` add ` il file.
6. Fare il commit con ` git commit -m "scrivere qualcosa" `

### Git clone, push e pull request

` git clone <indirizzo> <nome cartella> `

Questo comando clona un repository all' indirizzo specificato in nome cartella in modo da poterci lavoarare.

` git push origin some-branch `

Questo comando manda i cambiamenti fatti sul repository locale al repository remoto.

Importante! Ricordarsi di accettare i cambiamenti fatti anadando sul proprio account GitLab o GitHub e accettando la pull request!

` git pull origin some-branch `

Questo comando aggiorna il repository locale con il repository preso da remoto.
